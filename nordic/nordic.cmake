if (NOT EXISTS ${CMAKE_SOURCE_DIR}/nordic/pca/${NRF_DEVICE}.cmake)
    message(FATAL_ERROR "Unable to find device definition '${CMAKE_SOURCE_DIR}/nordic/pca/${NRF_DEVICE}.cmake'")
endif ()

if (NOT DEFINED NRF_COMPILER)
    set(NRF_COMPILER GNU_GCC_ARM)
endif ()

if ("${NRF_COMPILER}" STREQUAL "GNU_GCC_ARM")
    set(CMAKE_TOOLCHAIN_FILE ${CMAKE_SOURCE_DIR}/nordic/toolchains/arm-gcc-toolchain.cmake)
endif ()

# Find Nordic NRF5 sdk path
find_path(NRF5_SDK_PATH components/softdevice ${CMAKE_SOURCE_DIR}/sdk)
if (NOT EXISTS ${NRF5_SDK_PATH})
    message(FATAL_ERROR "Unable to find SDK set NRF5_SDK_PATH manually or upload it to CMAKE_SOURCE_DIR/sdk")
endif ()

# Find nordic utility to merge soft device with custom firmware
find_program(NRF_MERGEHEX_PATH mergehex)

# Find nordic utility to upload firmware to device
find_program(NRF_JPROG_PATH nrfjprog)

message(STATUS "Using SDK at ${NRF5_SDK_PATH}")
message(STATUS "Using ${NRF_MERGEHEX_PATH} to merge firmware")

include(nordic/pca/${NRF_DEVICE}.cmake)

set(NRF5_SDK_DIRECTORIES
        ${NRF5_SDK_DIRECTORIES}
        ${CMAKE_CURRENT_SOURCE_DIR}/mcu_config
        ${CMAKE_CURRENT_SOURCE_DIR}/mcu_config/config

        # General
        ${NRF5_SDK_PATH}/components/softdevice/common
        ${NRF5_SDK_PATH}/components/softdevice/${NRF_SOFTDEVICE_VERSION}/headers
        ${NRF5_SDK_PATH}/components/libraries/util
        ${NRF5_SDK_PATH}/components/toolchain/cmsis/include/
        ${NRF5_SDK_PATH}/components/boards/
        ${NRF5_SDK_PATH}/modules/nrfx
        ${NRF5_SDK_PATH}/modules/nrfx/mdk
        ${NRF5_SDK_PATH}/modules/nrfx/soc
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/src
        ${NRF5_SDK_PATH}/integration/nrfx/legacy
        ${NRF5_SDK_PATH}/external/utf_converter
        ${NRF5_SDK_PATH}/components/libraries/experimental_section_vars
        ${NRF5_SDK_PATH}/components/libraries/strerror
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/include
        ${NRF5_SDK_PATH}/integration/nrfx
        ${NRF5_SDK_PATH}/modules/nrfx/hal

        # Atomic
        ${NRF5_SDK_PATH}/components/libraries/atomic_fifo
        ${NRF5_SDK_PATH}/components/libraries/atomic_flags
        ${NRF5_SDK_PATH}/components/libraries/atomic
        ${NRF5_SDK_PATH}/components/libraries/mutex

        # Scheduler
        ${NRF5_SDK_PATH}/components/libraries/scheduler

        # Memory
        ${NRF5_SDK_PATH}/components/libraries/memobj
        ${NRF5_SDK_PATH}/components/libraries/balloc

        # Prog
        ${NRF5_SDK_PATH}/components/libraries/delay
        ${NRF5_SDK_PATH}/components/libraries/crc16
        ${NRF5_SDK_PATH}/components/libraries/ringbuf

        # Internal storage
        ${NRF5_SDK_PATH}/components/libraries/fds
        ${NRF5_SDK_PATH}/components/libraries/fstorage

        # Log
        ${NRF5_SDK_PATH}/components/libraries/log/src
        ${NRF5_SDK_PATH}/components/libraries/log
        ${NRF5_SDK_PATH}/external/segger_rtt
        ${NRF5_SDK_PATH}/external/fprintf

        # Power management
        ${NRF5_SDK_PATH}/components/libraries/pwr_mgmt

        # BSP
        ${NRF5_SDK_PATH}/components/libraries/bsp
        ${NRF5_SDK_PATH}/components/libraries/button

        # GPIO
        ${NRF5_SDK_PATH}/integration/nrfx/legacy

        # I2C / TWI (Ok)
        ${NRF5_SDK_PATH}/integration/nrfx
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/include

        # UART
        ${NRF5_SDK_PATH}/components/libraries/serial
        ${NRF5_SDK_PATH}/components/libraries/queue
        ${NRF5_SDK_PATH}/components/libraries/uart

        # Timer
        ${NRF5_SDK_PATH}/components/libraries/timer

        # BLE
        ${NRF5_SDK_PATH}/components/ble/common
        ${NRF5_SDK_PATH}/components/ble/nrf_ble_scan
        ${NRF5_SDK_PATH}/components/ble/nrf_ble_gq
        ${NRF5_SDK_PATH}/components/ble/peer_manager
        ${NRF5_SDK_PATH}/components/ble/nrf_ble_gatt
        ${NRF5_SDK_PATH}/components/ble/nrf_ble_scan

        # BLE Advertision
        ${NRF5_SDK_PATH}/components/ble/ble_advertising

        # BAS
        ${NRF5_SDK_PATH}/components/ble/ble_services/ble_bas

        # BLE Client
        ${NRF5_SDK_PATH}/components/ble/ble_db_discovery
        )

set(NRF5_SDK_SOURCES
        ${NRF5_SDK_SOURCES}

        # General
        ${NRF5_SDK_PATH}/components/libraries/util/app_error.c
        ${NRF5_SDK_PATH}/components/libraries/util/app_error_weak.c
        ${NRF5_SDK_PATH}/components/libraries/hardfault/nrf52/handler/hardfault_handler_gcc.c
        ${NRF5_SDK_PATH}/components/libraries/util/app_util_platform.c
        ${NRF5_SDK_PATH}/components/libraries/util/nrf_assert.c
        ${NRF5_SDK_PATH}/components/libraries/util/app_error_handler_gcc.c
        ${NRF5_SDK_PATH}/components/libraries/strerror/nrf_strerror.c
        ${NRF5_SDK_PATH}/integration/nrfx/legacy/nrf_drv_clock.c
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/src/nrfx_clock.c

        # Memory
        ${NRF5_SDK_PATH}/components/libraries/memobj/nrf_memobj.c
        ${NRF5_SDK_PATH}/components/libraries/balloc/nrf_balloc.c
        ${NRF5_SDK_PATH}/components/libraries/experimental_section_vars/nrf_section_iter.c

        # Prog
        ${NRF5_SDK_PATH}/components/libraries/crc16/crc16.c
        ${NRF5_SDK_PATH}/external/utf_converter/utf.c
        ${NRF5_SDK_PATH}/components/libraries/ringbuf/nrf_ringbuf.c

        # Internal storage
        ${NRF5_SDK_PATH}/components/libraries/fds/fds.c
        ${NRF5_SDK_PATH}/components/libraries/fstorage/nrf_fstorage.c
        ${NRF5_SDK_PATH}/components/libraries/fstorage/nrf_fstorage_sd.c

        # Log
        ${NRF5_SDK_PATH}/components/libraries/log/src/nrf_log_backend_uart.c

        # Power management
        ${NRF5_SDK_PATH}/components/libraries/pwr_mgmt/nrf_pwr_mgmt.c

        # Atomic
        ${NRF5_SDK_PATH}/components/libraries/atomic/nrf_atomic.c
        ${NRF5_SDK_PATH}/components/libraries/atomic_fifo/nrf_atfifo.c
        ${NRF5_SDK_PATH}/components/libraries/atomic_flags/nrf_atflags.c
        ${NRF5_SDK_PATH}/modules/nrfx/soc/nrfx_atomic.c

        # Scheduler
        ${NRF5_SDK_PATH}/components/libraries/scheduler/app_scheduler.c

        # Log
        ${NRF5_SDK_PATH}/components/libraries/log/src/nrf_log_frontend.c
        ${NRF5_SDK_PATH}/components/libraries/log/src/nrf_log_default_backends.c
        ${NRF5_SDK_PATH}/components/libraries/log/src/nrf_log_backend_rtt.c
        ${NRF5_SDK_PATH}/components/libraries/log/src/nrf_log_backend_serial.c
        ${NRF5_SDK_PATH}/components/libraries/log/src/nrf_log_str_formatter.c
        ${NRF5_SDK_PATH}/external/segger_rtt/SEGGER_RTT.c
        ${NRF5_SDK_PATH}/external/fprintf/nrf_fprintf.c
        ${NRF5_SDK_PATH}/external/fprintf/nrf_fprintf_format.c

        # BSP
        ${NRF5_SDK_PATH}/components/boards/boards.c
        ${NRF5_SDK_PATH}/components/libraries/bsp/bsp.c
        ${NRF5_SDK_PATH}/components/libraries/button/app_button.c

        # GPIO
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/src/nrfx_gpiote.c

        # I2C / TWI
        ${NRF5_SDK_PATH}/integration/nrfx/legacy/nrf_drv_twi.c
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/src/nrfx_twi.c
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/src/prs/nrfx_prs.c

        # UART
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/src/nrfx_uart.c
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/src/nrfx_uarte.c
        ${NRF5_SDK_PATH}/integration/nrfx/legacy/nrf_drv_uart.c
        ${NRF5_SDK_PATH}/components/libraries/queue/nrf_queue.c
        ${NRF5_SDK_PATH}/components/libraries/serial/nrf_serial.c

        # Timer
        ${NRF5_SDK_PATH}/components/libraries/timer/app_timer.c

        # SDH
        ${NRF5_SDK_PATH}/components/softdevice/common/nrf_sdh.c
        ${NRF5_SDK_PATH}/components/softdevice/common/nrf_sdh_soc.c
        ${NRF5_SDK_PATH}/components/softdevice/common/nrf_sdh_ble.c

        # BLE
        ${NRF5_SDK_PATH}/components/ble/peer_manager/auth_status_tracker.c
        ${NRF5_SDK_PATH}/components/ble/common/ble_conn_state.c
        ${NRF5_SDK_PATH}/components/ble/common/ble_srv_common.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/gatt_cache_manager.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/gatts_cache_manager.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/id_manager.c
        ${NRF5_SDK_PATH}/components/ble/nrf_ble_gatt/nrf_ble_gatt.c
        ${NRF5_SDK_PATH}/components/ble/nrf_ble_scan/nrf_ble_scan.c

        # Peer manager
        ${NRF5_SDK_PATH}/components/ble/peer_manager/peer_data_storage.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/peer_database.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/peer_id.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/peer_manager.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/peer_manager_handler.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/pm_buffer.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/security_dispatcher.c
        ${NRF5_SDK_PATH}/components/ble/peer_manager/security_manager.c

        # BLE Advertising
        ${NRF5_SDK_PATH}/components/ble/common/ble_advdata.c
        ${NRF5_SDK_PATH}/components/ble/ble_advertising/ble_advertising.c

        # BAS
        ${NRF5_SDK_PATH}/components/ble/ble_services/ble_bas/ble_bas.c

        # BLE Client
        ${NRF5_SDK_PATH}/components/ble/ble_db_discovery/ble_db_discovery.c

        #SAADC
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/src/nrfx_saadc.c

        # WDT
        ${NRF5_SDK_PATH}/modules/nrfx/drivers/src/nrfx_wdt.c
        )

set(NRF5_SDK_DEFINITIONS
        ${NRF5_SDK_DEFINITIONS}
        FLOAT_ABI_HARD
        NRF_CRYPTO_MAX_INSTANCE_COUNT=1
        SWI_DISABLE0
        uECC_ENABLE_VLI_API=0
        uECC_OPTIMIZATION_LEVEL=3
        uECC_SQUARE_FUNC=0
        uECC_SUPPORT_COMPRESSED_POINT=0
        uECC_VLI_NATIVE_LITTLE_ENDIAN=1
        )

set(NRF5_SDK_COMPILE_OPTIONS
        ${NRF5_SDK_COMPILE_OPTIONS}
        -mcpu=cortex-m4
        -mthumb -mabi=aapcs
        -mfloat-abi=hard -mfpu=fpv4-sp-d16
        -ffunction-sections -fdata-sections -fno-strict-aliasing
        -fno-builtin -fshort-enums
        -Wall -Werror
        )

message(${LINKER_SCRIPT})

set(NRF5_SDK_LINK_OPTIONS
        ${NRF5_SDK_LINK_OPTIONS}
        -mthumb -mabi=aapcs -L${NRF5_SDK_PATH}/modules/nrfx/mdk -T${LINKER_SCRIPT}
        -mcpu=cortex-m4
        -mfloat-abi=hard -mfpu=fpv4-sp-d16
        -Wl,--gc-sections
        --specs=nano.specs
        )

set(CFLAGS ${CFLAGS})
set(CPPFLAGS ${CPPFLAGS})
set(ASMFLAGS ${ASMFLAGS})
set(LDFLAGS ${ASMFLAGS})

# adds a target for comiling and flashing an executable
macro(firmware EXECUTABLE_NAME)
    set_target_properties(${EXECUTABLE_NAME} PROPERTIES SUFFIX ".out")

    # additional POST BUILD setps to create the .bin and .hex files
    add_custom_command(TARGET ${EXECUTABLE_NAME} POST_BUILD
            COMMAND ${CMAKE_SIZE_UTIL} ${EXECUTABLE_NAME}.out
            COMMAND ${CMAKE_OBJCOPY} -O binary ${EXECUTABLE_NAME}.out "${EXECUTABLE_NAME}.bin"
            COMMAND ${CMAKE_OBJCOPY} -O ihex ${EXECUTABLE_NAME}.out "${EXECUTABLE_NAME}.hex"
            COMMAND ${NRF_MERGEHEX_PATH} -m ${NRF5_SDK_PATH}/components/softdevice/${NRF_SOFTDEVICE_VERSION}/hex/${NRF_SOFTDEVICE_NUMBER}_softdevice.hex ${CMAKE_CURRENT_BINARY_DIR}/${EXECUTABLE_NAME}.hex -o ${CMAKE_BINARY_DIR}/${EXECUTABLE_NAME}_with_softdevice.hex
            COMMENT "post build steps for ${EXECUTABLE_NAME}")

endmacro()

