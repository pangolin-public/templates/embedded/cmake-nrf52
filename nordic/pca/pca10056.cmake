# Configure soft device
set(NRF_SOFTDEVICE_VERSION s140)
set(NRF_SOFTDEVICE_NUMBER s140_nrf52_7.0.1)
set(LINKER_SCRIPT ${CMAKE_CURRENT_SOURCE_DIR}/mcu_config/pca10056/gcc_nrf52840.ld)

# Use specific flags for pca10056
set(NRF5_SDK_DEFINITIONS
        ${NRF5_SDK_DEFINITIONS}
        BOARD_PCA10056
        NRF52840_XXAA
        )

# Add ASM files for pca10056
set(NRF5_SDK_SOURCES
        ${NRF5_SDK_SOURCES}
        ${NRF5_SDK_PATH}/modules/nrfx/mdk/gcc_startup_nrf52840.S
        ${NRF5_SDK_PATH}/modules/nrfx/mdk/system_nrf52840.c
        )
