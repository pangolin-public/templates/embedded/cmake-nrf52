# Configure soft device
set(NRF_SOFTDEVICE_VERSION s132)
set(NRF_SOFTDEVICE_NUMBER s132_nrf52_7.0.1)
set(LINKER_SCRIPT gcc_nrf52832.ld)
set(LINKER_SCRIPT ${CMAKE_CURRENT_SOURCE_DIR}/mcu_config/pca10040/gcc_nrf52832.ld)

# Use specific flags for pca10040
set(NRF5_SDK_DEFINITIONS
        ${NRF5_SDK_DEFINITIONS}
        BOARD_PCA10040
        NRF52832_XXAA
        )

# Add ASM files for pca10040
set(NRF5_SDK_SOURCES
        ${NRF5_SDK_SOURCES}
        ${NRF5_SDK_PATH}/modules/nrfx/mdk/gcc_startup_nrf52.S
        ${NRF5_SDK_PATH}/modules/nrfx/mdk/system_nrf52.c
        )
