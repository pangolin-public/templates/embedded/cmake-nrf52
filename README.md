# Template project to build Nordic NRF52 with cmake

## Configuration

Build a nordic project using CMake.
- Add ```.../gcc-arm-none-eabi/bin``` in your path
- Install nRF5 SDK version 16.0.0 as ${PROJECT}/sdk or set NRF5_SDK_PATH
- Add ```mergehex``` to your PATH or set NRF_MERGEHEX_PATH

## Execution

```
mkdir build
cd build
cmake ..
```

## Results

- **cmake-nrf-52.hex**: contains your custom code, used for partial upgrade
- **cmake-nrf-52_with_softdevice.hex**: contains your code and the Nordic soft device, must be used after erasing

## Debug CLion

- Create ```Embedded GDB Server``` configuration
- Set 'target remote' as ```:2331``` 
- Set 'GDB Server' as ```JLinkGDBServer```
- Set 'GDB Server args' as ```-select USB -device nRF52840_xxAA -endian little -if SWD -speed 4000 -noir -noLocalhostOnly```
- Run debug
